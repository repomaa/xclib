extern crate x11_clipboard;
extern crate xcb;

use x11_clipboard::Clipboard;
use x11_clipboard::error::{Error as X11Error};
use xcb::Atom;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::str;
use std::string::FromUtf8Error;
use std::time::Duration;
use std::error::Error as StdError;
use std::fmt;
use std::convert::From;
use std::mem::forget;


#[derive(Debug)]
struct StoreError {
    detail: String
}

#[derive(Debug)]
struct LoadError {
    detail: String
}

impl fmt::Display for LoadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("LoadError")
    }
}

impl fmt::Display for StoreError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("StoreError")
    }
}

impl StdError for StoreError {
    fn description(&self) -> &str {
        "Failed storing to clipboard"
    }
}

impl StdError for LoadError {
    fn description(&self) -> &str {
        "Failed loading from clipboard"
    }
}

macro_rules! impl_from {
    ( $type:ident from $err:ty ) => {
        impl From<$err> for $type {
            fn from(error : $err) -> $type {
                let detail = error.description().to_owned();
                $type { detail: detail }
            }
        }
    }
}

impl_from!(LoadError from X11Error);
impl_from!(StoreError from X11Error);
impl_from!(LoadError from FromUtf8Error);

fn store(clipboard: &Clipboard, selection : Atom, data: &str) -> Result<(), StoreError> {
    let utf8string = clipboard.setter.atoms.utf8_string;

    clipboard.store(selection, utf8string, data.as_bytes())?;
    Ok(())
}

fn load(clipboard: &Clipboard, selection : Atom) -> Result<(String), LoadError> {
    let utf8string = clipboard.setter.atoms.utf8_string;
    let property = clipboard.getter.atoms.property;

    let content = clipboard.load(selection, utf8string, property, Duration::from_secs(3))?;
    Ok(String::from_utf8(content)?)
}

#[repr(C)]
pub enum Status {
    OK,
    StoreError,
    LoadError,
    UTF8Error,
    ClipboardContextError
}

#[repr(C)]
pub enum Selection {
    PRIMARY,
    CLIPBOARD
}

fn atom_for_selection(clipboard: &Clipboard, selection: Selection) -> Atom {
    match selection {
        Selection::PRIMARY => clipboard.setter.atoms.primary,
        Selection::CLIPBOARD => clipboard.setter.atoms.clipboard
    }
}

#[no_mangle]
pub extern fn xclib_store(data: *const c_char, selection: Selection) -> Status {
    let c_str: &CStr = unsafe { CStr::from_ptr(data) };
    let string = match c_str.to_str() {
        Ok(string) => string,
        Err(_) => return Status::UTF8Error
    };

    let clipboard = match Clipboard::new() {
        Ok(clipboard) => clipboard,
        Err(_) => return Status::ClipboardContextError
    };

    match store(&clipboard, atom_for_selection(&clipboard, selection), string) {
        Ok(_) => Status::OK,
        Err(_) => Status::StoreError
    }
}

#[no_mangle]
pub extern fn xclib_load(data: *mut *const c_char, selection: Selection) -> Status {
    let clipboard = match Clipboard::new() {
        Ok(clipboard) => clipboard,
        Err(_) => return Status::ClipboardContextError
    };

    match load(&clipboard, atom_for_selection(&clipboard, selection)) {
        Ok(content) => {
            let pointer = CString::new(content).unwrap().into_raw();
            forget(pointer);

            unsafe {
                *data = pointer;
            }
            Status::OK
        },
        Err(_) => Status::LoadError
    }
}

#[no_mangle]
pub extern fn xclib_error_string(status : Status) -> *const c_char {
    let message = match status {
        Status::OK => "OK",
        Status::StoreError => "Storing failed",
        Status::LoadError => "Loading failed",
        Status::UTF8Error => "Failed to parse UTF8 string",
        Status::ClipboardContextError => "Failed create clipboard context"
    };

    let pointer = CString::new(message).unwrap().into_raw();
    forget(pointer);
    pointer
}
